# SMD Cartage

It's simple SMD storage box.

## Specifications 
1. Supports 1208, 0805 films
2. Anything same film size as the mentioned above
3. Dimensions - 50mm x 50mm inside, 53mm x 53mm outside

# Software used
1. FreeCAD - 3D parts

<img src="1.jpg" width="500">
<img src="2.jpg" width="500"> 
<img src="3.jpg" width="500"> 
<img src="4.jpg" width="500"> 
<img src="5.jpg" width="500">
<img src="6.jpg" width="500">


